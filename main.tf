terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
  backend "http" {
    }
}

provider "aws" {
  region = "us-east-2"
}

resource "aws_s3_bucket" "kellees3bucket" {
#   bucket = "kellees3bucket"

  tags = {
    Environment = "Dev"
  }

}


# resource "aws_s3_object" "object" {
#   bucket = aws_s3_bucket.kellees3bucket.bucket
#   key    = "g-hello-0.0.1-SNAPSHOT.jar"
#   source = "build/libs/g-hello-0.0.1-SNAPSHOT.jar"

#   source_hash = filemd5("build/libs/g-hello-0.0.1-SNAPSHOT.jar")
# }




# resource "aws_security_group" "kellee_security_group" {
#    name        = "kellee_security_group"
#   description = "Allow TLS inbound traffic"



#   ingress {
#     description      = "SSH"
#     from_port        = 22
#     to_port          = 22
#     protocol         = "tcp"
#     cidr_blocks      = ["0.0.0.0/0"]
#   }

#   ingress {
#     description      = "Spring boot port"
#     from_port        = 8080
#     to_port          = 8080
#     protocol         = "tcp"
#     cidr_blocks      = ["0.0.0.0/0"]
#   }

#   egress {
#     from_port        = 0
#     to_port          = 0
#     protocol         = "-1"
#     cidr_blocks      = ["0.0.0.0/0"]
#   }

#   tags = {
#     Name = "kellee_security_group"
#   }
# }

# resource "aws_iam_role_policy" "s3_get_object_policy_kellee" {
#     name = "s3_get_object_policy_kellee"
#     role = aws_iam_role.test_role2_kellee.id
#     policy = jsonencode({
#         "Version" = "2012-10-17"
#         "Statement" = [
#         {
#             "Effect"   : "Allow",
#             "Action" :[
#                 "s3:ListBucket",
#                 "s3:GetObject",
#             ],
#             "Resource" :[ 
#                 # "arn:aws:s3:::kellees3bucket/*",
#             "${aws_s3_bucket.kellees3bucket.arn}",
#             "${aws_s3_bucket.kellees3bucket.arn}/*",
#             ]
#         },
#           {
#             "Effect": "Allow",
#             "Action": "s3:ListAllMyBuckets",
#             "Resource": "*"
#         }

#         ]
#     })
# }

# resource "aws_iam_role" "test_role2_kellee" {
#   name = "test_role2_kellee"

#   assume_role_policy = jsonencode({
#     Version = "2012-10-17"
#     Statement = [
#       {
#         Action = "sts:AssumeRole"
#         Effect = "Allow"
#         Sid    = ""
#         Principal = {
#           Service = "ec2.amazonaws.com"
#         }
#       },
#     ]
#   })
# }

# resource "aws_iam_instance_profile" "test_profile_kellee" {
#   name = "test_profile_kellee"
#   role = aws_iam_role.test_role2_kellee.name
# }

# resource "aws_instance" "app_server_kellee" {
#     ami = "ami-02238ac43d6385ab3"
#     instance_type = "t2.micro"
#     # vpc_security_group_ids = [aws_security_group.kellee_security_group.id]
#     key_name = "KelleeEC2Key2"
#     iam_instance_profile = aws_iam_instance_profile.test_profile_kellee.name
#     security_groups = [ aws_security_group.kellee_security_group.name ]

#     user_data = <<EOF
#     #! /bin/bash
#     yum update -y
#     yum install -y java-17-amazon-corretto-headless
#     aws s3api get-object --bucket ${aws_s3_bucket.kellees3bucket.bucket} --key ${aws_s3_object.object.key} ${aws_s3_object.object.key}
#     java -jar ${aws_s3_object.object.key}
#     EOF


#     tags = {
#         Name = "app_server_kellee"
#     }
  
# }